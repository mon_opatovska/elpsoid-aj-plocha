#include "paintwidget.h"
using namespace std;

PaintWidget::PaintWidget(QWidget *parent)
	: QWidget(parent)
{
	setAttribute(Qt::WA_StaticContents);
	modified = false;
	painting = false;
	myPenWidth = 1;
	myPenColor = Qt::blue;
}

bool PaintWidget::openImage(const QString &fileName)
{
	body.clear();
	indexy.clear();
	QFile loaded(fileName);
	loaded.open(QIODevice::ReadOnly);

	QString riadok;
	QStringList slova;
	QPoint P;
	while (!riadok.contains("POINTS")) riadok = loaded.readLine();

	int pocet;
	pocet = riadok.split(" ")[1].toInt();
	printf("pocet %d",pocet);
	for (int i = 0; i < pocet; i++) {
		riadok = loaded.readLine();
		slova = riadok.split(" ");
		P.setX(slova[0].toDouble());
		P.setY(slova[1].toDouble());
		body.push_back(P);
		surZ.push_back(slova[2].toDouble());
	}
	printf("otvoreny");
	while (!riadok.contains("POLYGONS")) riadok = loaded.readLine();
	pocet_poly = riadok.split(" ")[1].toInt();

	for (int i = 0; i < pocet_poly; i++) {
		riadok = loaded.readLine();
		slova = riadok.split(" ");
		indexy.push_back(slova[1].toInt());
		indexy.push_back(slova[2].toInt());
		indexy.push_back(slova[3].toInt());
	}
	loaded.close();

	return true;
}

bool PaintWidget::newImage(int x, int y)
{
	QImage loadedImage(x,y,QImage::Format_RGB32);
	loadedImage.fill(qRgb(255, 255, 255));
	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::saveImage(const QString &fileName)
{
	QImage visibleImage = image;
	resizeImage(&visibleImage, size());

	if (visibleImage.save(fileName,"png")) {
		modified = false;
		return true;
	}
	else {
		return false;
	}
}

void PaintWidget::setPenColor(const QColor &newColor)
{
	myPenColor = newColor;
}

void PaintWidget::setPenWidth(int newWidth)
{
	myPenWidth = newWidth;
}

void PaintWidget::clearImage()
{
	image.fill(qRgb(255, 255, 255));
	modified = true;
	update();
}

void PaintWidget::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton) {
		lastPoint = event->pos();
		painting = true;
		QPainter painter(&image);
		painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
		painter.drawPoint(lastPoint);
		BodyPlocha.push_back(QVector3D(lastPoint.x(),lastPoint.y(),0));
		update();
	}
}

void PaintWidget::mouseDoubleClickEvent(QMouseEvent *event)
{

}

void PaintWidget::mouseMoveEvent(QMouseEvent *event)
{
	if ((event->buttons() & Qt::RightButton) && painting)
		drawLineTo(event->pos());

	
}

void PaintWidget::mouseReleaseEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton) {
		lastPoint = event->pos();
		painting = true;
	}

	if (event->button() == Qt::RightButton && painting) {
		drawLineTo(event->pos());
		painting = true;
		rightclick = true;
	}

	if (rightclick == true) {
		posunutie();
		update();
	}
	rightclick = false;
}

void PaintWidget::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QRect dirtyRect = event->rect();
	painter.drawImage(dirtyRect, image, dirtyRect);
}

void PaintWidget::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
}

void PaintWidget::drawLineTo(const QPoint &endPoint)
{
	QPainter painter(&image);
	//	QColor C(200, 255, 255);
	//	painter.setPen(QPen(C, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	//	painter.drawLine(lastPoint2, endPoint);
	modified = true;

	int rad = (myPenWidth / 2) + 2;
	update(QRect(lastPoint2, endPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
	lastPoint2 = endPoint;
	posunutie_body.push_back(lastPoint2);
}

void PaintWidget::resizeImage(QImage *image, const QSize &newSize)
{
	if (image->size() == newSize)
		return;

	QImage newImage(newSize, QImage::Format_RGB32);
	newImage.fill(qRgb(255, 255, 255));
	QPainter painter(&newImage);
	painter.drawImage(QPoint(0, 0), *image);
	*image = newImage;
}

int PaintWidget::selectKth(int * data, int s, int e, int k)
{
	// 5 or less elements: do a small insertion sort
	if (e - s <= 5)
	{
		for (int i = s + 1; i < e; i++)
			for (int j = i; j > 0 && data[j - 1] > data[j]; j--) std::swap(data[j], data[j - 1]);
		return s + k;
	}

	int p = (s + e) / 2; // choose simply center element as pivot

						 // partition around pivot into smaller and larger elements
	std::swap(data[p], data[e - 1]); // temporarily move pivot to the end
	int j = s;  // new pivot location to be calculated
	for (int i = s; i + 1 < e; i++)
		if (data[i] < data[e - 1]) std::swap(data[i], data[j++]);
	std::swap(data[j], data[e - 1]);

	// recurse into the applicable partition
	if (k == j - s) return s + k;
	else if (k < j - s) return selectKth(data, s, j, k);
	else return selectKth(data, j + 1, e, k - j + s - 1); // subtract amount of smaller elements from k
}

void PaintWidget::generuj(int rovnobezky, int poludniky) //n-rovnobezka, m-poludnik
{
	body.clear();
	indexy.clear();
	QPoint A;
	double alfa = 0, beta = 0;
	int a = 10, b = 15, c = 20; //polosi
	int xs = image.width();
	int ys = image.height();
	rovnobezky++;

	fstream file;
	file.open("elipsoid1.vtk", fstream::out);

	file << "# vtk DataFile Version 3.0" << endl;
	file << "vtk output" << endl << "ASCII" << endl << "DATASET POLYDATA" << endl;

	double poma = 2 * M_PI / poludniky;
	double pomb = M_PI / rovnobezky;
	
	SUR tmp; //struktura so suradnicami x,y,z

	file << "POINTS " << poludniky*(rovnobezky + 1) << " float" << endl;

	for (int i = 0; i <= rovnobezky; i++)
	{
		alfa = 0;
		for (int j = 0; j < poludniky; j++)
		{
			tmp.x = (xs/2)+(a*cos(alfa)*sin(beta))*10.0;
			tmp.y = (ys/2)+(b*sin(alfa)*sin(beta))*10.0;
			tmp.z = (c*cos(beta))*10.0;

			A.setX(tmp.x);
			A.setY(tmp.y);
			body.push_back(A);
			surZ.push_back(tmp.z);

			alfa += poma;
		}
		beta += pomb;
	}

	for (int i = 0; i < poludniky*(rovnobezky + 1); i++)
	{
		file << body[i].x() << " " << body[i].y() << " " << surZ[i] << endl;
	}

	pocet_poly = ((rovnobezky*poludniky) - poludniky) * 2;
	file << "POLYGONS " << pocet_poly << " " << 4 * pocet_poly << endl;
	
	rovnobezky--;

	for (int i = poludniky; i < poludniky * 2; i++)
	{
		if (i == (poludniky * 2) - 1)
		{
			file << "3 " << poludniky << " ";
			indexy.push_back(poludniky);
		}
			
		else
		{
			file << "3 " << i + 1 << " ";
			indexy.push_back(i + 1);
		}
			
		file << i << " 0" << endl;
		indexy.push_back(i);
		indexy.push_back(0);

	}

	 
	for (int i = poludniky; i < (pocet_poly / 2); i++)
	{
		if (i%poludniky == poludniky - 1)
		{
			i -= poludniky;
			file << "3 " << i + poludniky << " " << i + 1 << " " << i + 2 * poludniky << endl;
			file << "3 " << i + 1 << " " << i + poludniky + 1 << " " << i + 2 * poludniky << endl;
			indexy.push_back(i + poludniky);
			indexy.push_back(i + 1);
			indexy.push_back(i + 2*poludniky);
			indexy.push_back(i + 1);
			indexy.push_back(i + poludniky+1);
			indexy.push_back(i + 2*poludniky);
			i += poludniky;
		}
		else
		{
			file << "3 " << i << " " << i + 1 << " " << i + poludniky << endl;
			file << "3 " << i + 1 << " " << i + poludniky + 1 << " " << i + poludniky << endl;
			indexy.push_back(i);
			indexy.push_back(i + 1);
			indexy.push_back(i + poludniky);
			indexy.push_back(i + 1);
			indexy.push_back(i + poludniky+1);
			indexy.push_back(i + poludniky);
		}

	}

	for (int i = pocet_poly / 2; i < poludniky*(rovnobezky + 1); i++)
	{

		if (i == (poludniky*(rovnobezky + 1)) - 1)
		{
			file << "3 " << i << " " << pocet_poly / 2 << " " << poludniky*(rovnobezky + 1) << endl;
			indexy.push_back(i);
			indexy.push_back(pocet_poly / 2);
			indexy.push_back(poludniky*(rovnobezky + 1));
		}
		else
		{
			file << "3 " << i << " " << i + 1 << " " << poludniky*(rovnobezky + 1) << endl;
			indexy.push_back(i);
			indexy.push_back(i + 1);
			indexy.push_back(poludniky*(rovnobezky + 1));
		}
	}
	
	Zpom = surZ;
	Zpom1 = surZ;

	file.close();
}

void PaintWidget::rotuj(double PHI, double THETA, bool priemet)
{
	double phi = M_PI*PHI / 180.0;
	double theta = M_PI*THETA / 180.0;
	int xs = image.width();
	int ys = image.height();
	
	if (priemet == 0) //rotacia rovnobezneho priemetu
	{
		for (int i = 0; i < Bodiky.size(); i++) 
		{
			int xx, yy;
			int it = indexy[i];
			xx = body[it].x() - (xs/2);
			yy = body[it].y() - (ys / 2);
			
			Bodiky[i].setX((xx*cos(phi) - yy*sin(phi)) + (xs / 2));
			Bodiky[i].setY((xx*sin(phi)*cos(theta) + yy*cos(phi)*cos(theta) - surZ[it] * sin(theta)) + (ys / 2));
			Zpom[i] = ((xx*sin(phi)*sin(theta)) + (xs / 2)) + ((yy*cos(theta)*sin(phi)) + (ys / 2)) + surZ[it] * cos(theta);
		}
	}

	if (priemet == 1)//rotacia stredoveho priemetu
	{
		for (int i = 0; i < Bodiky.size(); i++)
		{
			int xx, yy;
			int it = indexy[i];
			xx = ((((body[it].x() - (xs / 2)) * dz) / (dz - surZ[it])) + (xs / 2)) - (xs / 2);
			yy = ((((body[it].y() - (ys / 2)) * dz) / (dz - surZ[it])) + (ys / 2)) - (ys / 2);


			Bodiky[i].setX((xx*cos(phi) - yy*sin(phi)) + (xs / 2));
			Bodiky[i].setY((xx*sin(phi)*cos(theta) + yy*cos(phi)*cos(theta) - Zpom[it]*sin(theta)) + (ys / 2));
			Zpom1[i] = ((xx*sin(phi)*sin(theta)) + (xs / 2)) + ((yy*cos(theta)*sin(phi)) + (ys / 2)) + Zpom[it] * cos(theta);
		}
	}
}

bool PaintWidget::kontrola(QPoint bod)//zisti ci sa bod nachadza na platne
{
	int xs = image.width();
	int ys = image.height();

	if ((bod.x() <= xs) && (bod.y() <= ys))
		return true;
}

void PaintWidget::zBuffer(QVector<QColor>Farba, int zsur)
{
	for (int i = 0; i < image.width(); i++)
	{
		for (int j = 0; j < image.height(); j++)
		{
			F[i][j] = QColor(255, 255, 255);
			Z[i][j] = INFINITY;
		}
	}

	for (int i = 0; i < pocet_poly; i++)
	{
		for (int j = 0; j < body.size(); j++)
		{
			if (Z[i][j] > zsur)
			{
				Z[i][j] = zsur;
				F[i][j] = Farba[j];
			}
		}
	}
}

void PaintWidget::Phongov_osv_model(bool moznost)
{
	
	QVector<SUR>R,Is,Id,Ia;
	SUR p_r, p_is, p_id, p_ia,vysl_i;
	double w1,w2,w3;
	//konstantne tienovanie
	if (moznost == 0)
	{
		for (int i = 0; i < Bodiky.size(); i++)
		{	
			//vypocet normaly
			//u2*v3 - u3*v2
			w1 = ((Bodiky[i + 1].y() - Bodiky[i].y())*(surZ[i + 2] - surZ[i + 1])) - ((surZ[i + 1] - surZ[i])*(Bodiky[i + 2].y() - Bodiky[i + 1].y()));
			//u3*v1 - u1*v3
			w2 = ((surZ[i + 1] - surZ[i])*(Bodiky[i + 2].x() - Bodiky[i + 1].x())) - ((Bodiky[i + 1].x() - Bodiky[i].x())*(surZ[i + 2] - surZ[i + 1]));
			//u1*v2 - u2*v1
			w3 = ((Bodiky[i + 1].x() - Bodiky[i].x())*(Bodiky[i + 2].y() - Bodiky[i + 1].y())) - ((Bodiky[i + 1].y() - Bodiky[i].y())*(Bodiky[i + 2].x() - Bodiky[i + 1].x()));

			//odrazovy luc
			p_r.x = 2 * (0 * w1)*w1 - 0;
			p_r.y = 2 * (0 * w2)*w2 - 0;
			p_r.z = 2 * (dz * w3)*w3 - dz;
			R.push_back(p_r);

			//zrkadlova zlozka
			p_is.x = ILr*rs*pow(0 * p_r.x, h);
			p_is.y = ILg*rs*pow(0 * p_r.y, h);
			p_is.z = ILb*rs*pow(0 * p_r.z, h);
			Is.push_back(p_is);

			//difuzna zlozka
			p_id.x = ILr*rd * 0 * w1;
			p_id.y = ILg*rd * 0 * w2;
			p_id.z = ILb*rd * 0 * w3;
			Id.push_back(p_id);

			//ambientna zlozka
			p_ia.x = IOr*ra;
			p_ia.y = IOg*ra;
			p_ia.z = IOb*ra;
			Ia.push_back(p_ia);

			//vysledna intenzita (asi RGB)
			vysl_i.x = p_is.x + p_id.x + p_ia.x;	//R
			vysl_i.y = p_is.y + p_id.y + p_ia.y;	//G
			vysl_i.z = p_is.z + p_id.z + p_ia.z;	//B
			QColor(vysl_i.x, vysl_i.y, vysl_i.z);
			I.push_back(QColor(vysl_i.x, vysl_i.y, vysl_i.z));
		}
	}
}

void PaintWidget::TH(QVector<QPoint>troj)
{
	printf("%d troj size\n", troj.size());
	tabulkaHran.clear();
	double w, x1, x2, y1, y2, dx, dy;
	QVector<double>row;

	for (int i = 0; i < troj.size(); i++)
	{
		printf("%d ", troj[i].y());
	
		/*if (i == troj.size() - 1) //posledny
		{
			x1 = troj[i].x();
			y1 = troj[i].y();
			x2 = troj[0].x();
			y2 = troj[0].y();

			printf("posledny\n %.0lf %.0lf %.0lf %.0lf\n", x1, y1, x2, y2);

			if (y1 == y2)
				ii.push_back(i);
		}

		else
		{
			if (troj[i].y() != troj[i + 1].y())
			{
				x1 = troj[i].x();
				y1 = troj[i].y();
				x2 = troj[i + 1].x();
				y2 = troj[i + 1].y();
			}


		//	printf("%.0lf %.0lf %.0lf %.0lf\n", x1, y1, x2, y2);

//			if (y1 == y2)
			//	ii.push_back(i);
	//	}

		//vynecha vodorovne hrany
		if ((y2 - y1) != 0)
		{
			//upravi orientaciu hran
			//if (y1 > y2)
			//{
				//std::swap(x1, x2);
				//std::swap(y1, y2);
			//}
			dy = y2 - y1;
			w = (x2 - x1) / dy;
			row.push_back(x1);
			row.push_back(y1);
			row.push_back(y2 - 1);
			row.push_back(w);
			tabulkaHran.push_back(row);
			row.clear();
		}
	*/
	}
	printf("\n");
}

void PaintWidget::sort()
{
	for (int i = 0; i < tabulkaHran.size(); i++)
	{
		for (int j = 0; j < tabulkaHran.size(); j++)
		{
			
			if (tabulkaHran[i][0] < tabulkaHran[j][0])	
			{
				QVector<double> tmp = tabulkaHran[i];
				tabulkaHran[i] = tabulkaHran[j];
				tabulkaHran[j] = tmp;
			}
	
			else if (tabulkaHran[i][0] == tabulkaHran[j][0])
			{
				if (tabulkaHran[i][3] < tabulkaHran[j][3])	
				{
					QVector<double> tmp = tabulkaHran[i];
					tabulkaHran[i] = tabulkaHran[j];
					tabulkaHran[j] = tmp;
				}
			}
		}
	}
}

void PaintWidget::vypln_troj()
{
	QPainter painter(&image);
	bodicky = body;
	QVector<int>zet;
	QVector<QPoint> vrchol1;
	QVector<QPoint> vrchol2;

	printf("pocet vrcholov body.size %d\n", body.size());
	for (int i = 0; i < pocet_poly; i++)
	{
		printf("zoradovanie vrcholov \n");
		printf("i %d\n", i);
		//zoradovanie vrcholov trojuholnika, aby sme nasli stredny bod
		int it = indexy[i];

		printf("bodicky[it] %d bodicky[it + 1] %d bodicky[it + 2] %d\n", bodicky[it].y(), bodicky[it + 1].y(), bodicky[it + 2].y());

		//ziadne dva body nelezia na priamke
		if (((bodicky[it].y() != bodicky[it + 2].y()) && (bodicky[it + 1].y() != bodicky[it + 2].y())) &&
			((bodicky[it].y() != bodicky[it + 2].y()) && (bodicky[it + 1].y() != bodicky[it].y())) &&
			((bodicky[it + 1].y() != bodicky[it + 2].y()) && (bodicky[it].y() != bodicky[it + 1].y())))
		{

			if (bodicky[it].y() > bodicky[it + 2].y())
			{
				printf("bodicky[it] %d bodicky[it+2] %d\n", bodicky[it].y(), bodicky[it + 2].y());
				swap(bodicky[it], bodicky[it + 2]);
				printf("bodicky[it] %d bodicky[it+2] %d\n", bodicky[it].y(), bodicky[it + 2].y());
			}

			if (bodicky[it].y() > bodicky[it + 1].y())
			{
				printf("bodicky[it] %d bodicky[it+1] %d\n", bodicky[it].y(), bodicky[it + 1].y());
				swap(bodicky[it], bodicky[it + 1]);
				printf("bodicky[it] %d bodicky[it+1] %d\n", bodicky[it].y(), bodicky[it + 1].y());
			}

			if (bodicky[it + 1].y() > bodicky[it + 2].y())
			{
				printf("bodicky[it+1] %d bodicky[it+2] %d\n", bodicky[it + 1].y(), bodicky[it + 2].y());
				swap(bodicky[it + 1], bodicky[it + 2]);
				printf("bodicky[it+1] %d bodicky[it+2] %d\n", bodicky[it + 1].y(), bodicky[it + 2].y());
			}

			zet.push_back(surZ[it]);
			zet.push_back(surZ[it + 1]);
			zet.push_back(surZ[it + 2]);

			stredne.push_back(bodicky[it + 1]);
			vrchol1.push_back(bodicky[it]);
			vrchol2.push_back(bodicky[it + 2]);

			printf("stredne %d\n", bodicky[it + 1].y());
		}

		if ((bodicky[it].y() == bodicky[it + 1].y()) && (bodicky[it].y() != bodicky[it + 2].y()) ||
			(bodicky[it].y() == bodicky[it + 2].y()) && (bodicky[it].y() != bodicky[it + 1].y()) ||
			(bodicky[it + 1].y() == bodicky[it + 2].y()) && (bodicky[it + 1].y() != bodicky[it].y())) //dva body su na priamke
		{
			zet.push_back(surZ[it]);
			zet.push_back(surZ[it + 1]);
			zet.push_back(surZ[it + 2]);

			printf("dva su rovnake zapisali sa Z suradnice\n");
		}

		//vsetky lezia na priamke aj ked to by nemalo nastat
		if ((bodicky[it].y() == bodicky[it + 2].y()) && (bodicky[it + 1].y() == bodicky[it + 2].y()))
		{
			
			printf("vsetky su stejne\n");
			continue;
		}
	}
	
	for (int i = 0; i < stredne.size(); i++)
	{
		//hladame bod D (linearna interpolacia)
		QPoint pomD;
		pomD.setY(stredne[i].y());
		pomD.setX(vrchol1[i].x() + ((stredne[i].y() - vrchol1[i].y())*
			((vrchol2[i].x() - vrchol1[i].x()) / (vrchol2[i].y() - vrchol1[i].y()))));//vzorec

		D.push_back(pomD);

		//printf("D %d %d\n", pomD.x(), pomD.y());
	}
		
	for (int i = 0; i < stredne.size(); i++)
	{

		//ulozenie do vektorov trojuholniky ABD,ACD
		ABD.push_back(stredne[i]);
		ABD.push_back(vrchol2[i]);
		ABD.push_back(D[i]);
		ABDz.push_back(zet[i + 1]); ABDz.push_back(zet[i]); ABDz.push_back((zet[i]+zet[i + 2])/2);
		
		ACD.push_back(stredne[i]);
		ACD.push_back(vrchol1[i]);
		ACD.push_back(D[i]);
		ACDz.push_back(zet[i + 1]); ACDz.push_back(zet[i + 2]); ACDz.push_back((zet[i] + zet[i + 2]) / 2);

	}

	for (int i = 0; i < ABD.size(); i++)
	{
		//	printf("ABD %d %d     ", ABD[i].x(),ABD[i].y());
		printf("ACD %d %d     ", ACD[i].x(), ACD[i].y());
		if (i % 3 == 2) printf("\n\n");
	}

	//vyplnenie 1. trojuholnika
	TH(ACD);

	for (int i = 0; i < tabulkaHran.size(); i++)
	{
		for (int j = 0; j < 4; j++)
		{
			printf("TH %.2lf ", tabulkaHran[i][j]);
		}
		printf("\n");
	}
	
	//sort();
	
	/*for (int i = 0; i < tabulkaHran.size(); i++)
	{
		for (int j = 0; j < 4; j++)
		{
			printf("%d TH %.2lf ", i, tabulkaHran[i][j]);
		}
		printf("\n");
	}*/

	//printf("%d tabulka hran\n", tabulkaHran.size());
	
/*	int ya = tabulkaHran[1][0];
	for (int i = 0; i < ACD.size(); i++)
	{
		Phongov_osv_model(moznost);
		//QColor C(I[i]);
		QColor C(255,0,0);
		painter.setPen(QPen(C, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
		painter.drawPoint(ACD[i + 1]);	//vykreslenie bodu C
		zBuffer(I, ACDz[i + 1]);		//zapisanie do z-buffera

		//vypocitanie priesecnika
		QVector<int>priesecniky;
		priesecniky.clear();
		for (int j = 0; j < tabulkaHran.size(); j = j + 2)
		{
			int xa = 0, xaa = 0;
			xa = tabulkaHran[j][3] * ya - tabulkaHran[j][3] * tabulkaHran[j][0] + tabulkaHran[j][1];
			xaa = tabulkaHran[j + 1][3] * ya - tabulkaHran[j + 1][3] * tabulkaHran[j + 1][0] + tabulkaHran[j + 1][1];
			priesecniky.push_back(xa);
			priesecniky.push_back(xaa);
			painter.drawPoint(xa,ya);
			zBuffer(I, (ACDz[i + 1] + ACDz[i]) / 2.0);
			painter.drawPoint(xaa,ya);
			zBuffer(I, (ACDz[i + 1] + ACDz[i+2]) / 2.0);
			painter.drawLine(xa, ya, xaa, ya);
			zBuffer(I, (xa + xaa) / 2.0);
		}
		ya = ya + tabulkaHran[i][3];
	}
	/*
	//vyplnenie 2. trojuholnika
	TH(ABD);
	int ya2 = tabulkaHran[0][0];
	for (int i = 0; i < ABD.size(); i++)
	{
		Phongov_osv_model(moznost);
		//QColor C(I[i]);
		QColor C(255, 0, 0);
		painter.setPen(QPen(C, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
		painter.drawPoint(ABD[i]);	//vykreslenie bodu A
		zBuffer(I, ABDz[i]);		//zapisanie do z-buffera
		painter.drawPoint(ABD[i + 2]);	//vykreslenie bodu D
		zBuffer(I, ABDz[i + 2]);		//zapisanie do z-buffera
		painter.drawLine(ABD[i], ABD[i + 2]);

		//vypocitanie priesecnika
		QVector<int>priesecniky;
		priesecniky.clear();
		for (int j = 0; j < tabulkaHran.size(); j = j + 2)
		{
			int xa = 0, xaa = 0;
			xa = tabulkaHran[j][3] * ya2 - tabulkaHran[j][3] * tabulkaHran[j][0] + tabulkaHran[j][1];
			xaa = tabulkaHran[j + 1][3] * ya2 - tabulkaHran[j + 1][3] * tabulkaHran[j + 1][0] + tabulkaHran[j + 1][1];
			priesecniky.push_back(xa);
			priesecniky.push_back(xaa);
			painter.drawPoint(xa, ya);
			zBuffer(I, (ABDz[i + 1] + ABDz[i]) / 2.0);
			painter.drawPoint(xaa, ya2);
			zBuffer(I, (ABDz[i + 1] + ABDz[i + 2]) / 2.0);
			painter.drawLine(xa, ya2, xaa, ya2);
			zBuffer(I, (xa + xaa) / 2.0);
		}
		ya2 = ya2 + tabulkaHran[i][3];
		painter.drawPoint(ABD[i + 1]);	//vykreslenie bodu B
		zBuffer(I, ABDz[i + 1]);		//zapisanie do z-buffera
	}*/
}

void PaintWidget::DDA(QVector<QPoint>b)
{
	QPainter painter(&image);
	QColor C(255, 0, 0);
	painter.setPen(QPen(C, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.setBrush(QBrush(C));
	
	double dx, dy, steps, xx, yy, x, y;
	int x1, x2, y1, y2, x0, y0;
	
	for (int i = 0; i < b.size() - 1; i++)
	{
		x1 = b[i].x();
		y1 = b[i].y();
		x2 = b[i + 1].x();
		y2 = b[i + 1].y();
	
		dx = x2 - x1;
		dy = y2 - y1;

		if (fabs(dx) > fabs(dy))//smernica od 0 do 1
			steps = fabs(dx);
		
		else steps = fabs(dy);

		xx = dx / steps; //jedno smernica a druhe = 1 
		yy = dy / steps;

		x = x1;
		y = y1;

		for (int i = 1; i <= steps; i++)//rozdiel 
		{
			painter.drawEllipse((int)x, (int)y, 2, 2);
			x += xx;
			y += yy;
		}
		painter.drawEllipse(x, y, 2, 2);
	}
	
	update();
}

void PaintWidget::otocenie_vlavo(int uhol)
{
	double u = M_PI / double(uhol);
	
	for (int i = 0; i < body.size(); i++)
	{
		double dlzkax = (double)body[i].x() - (double)body[0].x();
		double dlzkay = (double)body[i].y() - (double)body[0].y();

		body.replace(i, QPoint((int)round((dlzkax * cos(u)) + dlzkay * sin(u)) + body[0].x(),
			body[0].y() + (int)round((-dlzkax * sin(u)) + dlzkay * cos(u))));
	}
		
	update();
}

void PaintWidget::skalovanie(float koef)
{
	QPainter painter(&image);
	
	for (int i = 0; i < body.size(); i++)
	{
		body.replace(i, QPoint(body[0].x() + (int)round(((double)body[i].x() - (double)body[0].x()) * koef),
			body[0].y() + (int)round(((double)body[i].y() - (double)body[0].y()) * koef)));
	}

	update();
}

void PaintWidget::otocenie_vpravo(int uhol)
{
	double u = M_PI / double(uhol);

	for (int i = 0; i < body.size(); i++)
	{
		double dlzkax = (double)body[i].x() - (double)body[0].x();
		double dlzkay = (double)body[i].y() - (double)body[0].y();

		body.replace(i, QPoint((int)round((dlzkax * cos(u)) - dlzkay * sin(u)) + body[0].x(),
			body[0].y() + (int)round((dlzkax * sin(u)) + dlzkay * cos(u))));
	}

	update();
}

void PaintWidget::posunutie()
{
	clearImage();
	body_pom.clear();
	Bodiky.clear();

	QPoint Z, K;
	Z = posunutie_body[0];
	K = posunutie_body[posunutie_body.size() - 1];

	int posunX = K.x() - Z.x();
	int posunY = K.y() - Z.y();
	for (int i = 0; i < body.size(); i++)
	{
		int noveX, noveY;
		noveX = body[i].x() + posunX;
		noveY = body[i].y() + posunY;
		body_pom.push_back(QPoint(noveX, noveY));
	}
	
	for (int i = 0; i < indexy.size(); i++)
	{
		int it = indexy[i];
		Bodiky.push_back(body_pom[it]);
	}
	DDA(Bodiky);
	
	body.clear();
	body = body_pom;
	posunutie_body.clear();
}

void PaintWidget::plocha(int pocet_deleni, int pocet_kriviek)//rovnobezky poludniky
{
	
	//fstream file;
	
	QVector<QVector3D>KrivkoveBody;
	
	
	int delta = pocet_deleni;
	int pocet_bodov = BodyPlocha.size();
	float t = 0;
	QPointF B1 = QPointF(BodyPlocha[0].toPointF());

	KrivkoveBody.push_back(QVector3D(B1.x(), B1.y(), 0));

	QVector<QPointF> P1, P2;

	if (pocet_bodov < 2)
		return;

	P1.resize(pocet_bodov);
	P2.resize(pocet_bodov);

	QVector<QPoint> pomocny;

	for (int i = 0; i < pocet_bodov; i++) //priradenie povodnych bodov do vektora
		P1[i] = (QPointF)BodyPlocha[i].toPointF();

	while (t < 1)
	{
		t = t + 1.0 / delta;

		for (int j = 1; j < pocet_bodov; j++)
		{
			for (int i = 0; i < pocet_bodov - j; i++)
				P2[i] = (1 - t) * P1[i] + t *  P1[i + 1];

			P1 = P2;
		}

		pomocny.push_back(B1.toPoint());
		pomocny.push_back(P1[0].toPoint());
		
		B1 = P1[0];
		
		KrivkoveBody.push_back(QVector3D(B1.x(), B1.y(), 0)); //priradenie bodov na krivke do vektora
				
		for (int i = 0; i < pocet_bodov; i++)
			P1[i] = (QPointF)BodyPlocha[i].toPointF();

	}

	DDA(pomocny);
	pomocny.clear();

	BodyPlocha.clear();

	int size = KrivkoveBody.size();

	for (int i = 1; i < pocet_kriviek; i++)
	{
		for (int j = 0; j < size; j++)
		{
			QVector3D pom;

			pom.setX(KrivkoveBody[j].x());
			pom.setY(KrivkoveBody[j].y());
			pom.setZ(KrivkoveBody[j].z() + i*15);
			
			KrivkoveBody.push_back(pom);
		}
	}

	pocet_poly = 2 * pocet_deleni * (pocet_kriviek - 1);

	int zac = 0;
	for (int i = 0; i < pocet_kriviek - 1; i++)
	{
		for (int j = 0; j < pocet_deleni; j++)
		{
			indexy.push_back(zac + j);
			indexy.push_back(zac + j + 1);
			indexy.push_back(zac + j + 1 + pocet_deleni);
			indexy.push_back(zac + j);

			indexy.push_back(zac + j + 1);
			indexy.push_back(zac + j + 2 + pocet_deleni);
			indexy.push_back(zac + j + 1 + pocet_deleni);
			indexy.push_back(zac + j + 1);
		}
		zac = zac + pocet_deleni + 1;
	}

	for (int i = 0; i < KrivkoveBody.size(); i++)
	{
		//zetove suradnice bodov
		surZ.push_back(KrivkoveBody[i].z()); 
		
		//suradnice x y
		body.push_back(QPoint(KrivkoveBody[i].toPoint()));
	}

	Zpom = surZ;
}